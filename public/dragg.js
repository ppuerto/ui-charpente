// Click and Drag an object extending a p5.Vector

class Draggable extends p5.Vector {
  constructor(x, y) {
    super(x, y)

    this.dragging = false; // Is the object being dragged?
    this.rollover = false; // Is the mouse over the ellipse?

    this.r = 15;
  }

  over() {
    // Is mouse over object
    if (dist(this.x, this.y, mouseX, mouseY) < this.r) {
      this.rollover = true;
    } else {
      this.rollover = false;
    }

  }

  update() {

    // Adjust location if being dragged
    if (this.dragging) {
      this.x = mouseX + this.offsetX;
      this.y = mouseY + this.offsetY;
    }

  }


  show() {
    push()
    stroke(0);
    // Different fill based on state
    if (this.dragging) {
      fill(50);
    } else if (this.rollover) {
      fill(100);
    } else {
      fill(175, 200);
    }
    ellipse(this.x, this.y, this.r);
    pop()
  }

  pressed() {

    // Did I click on it ?
    if (dist(this.x, this.y, mouseX, mouseY) < this.r) {
      this.dragging = true;
      // If so, keep track of relative location of click to center
      this.offsetX = this.x - mouseX;
      this.offsetY = this.y - mouseY;
    }
  }

  released() {

    // Quit dragging
    this.dragging = false;
  }
}