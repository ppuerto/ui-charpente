function draw_line(a, b, s=1) {
	push();
	strokeWeight(s)
	line(a.x, a.y, b.x, b.y)
	pop();
}

function draw_lerp(a, b, s=1) {
	push();
	strokeWeight(s)
	drawingContext.setLineDash([5, 15]);
	line(a.x, a.y, b.x, b.y)
	pop();
}

function intersect_point(p1A, p1B, p2A, p2B) {
	let den = ((p1A.x-p1B.x)*(p2A.y-p2B.y))-((p1A.y-p1B.y)*(p2A.x-p2B.x));
	
	let x = ((p1A.x*p1B.y-p1A.y*p1B.x)*(p2A.x-p2B.x) - (p1A.x-p1B.x)*(p2A.x*p2B.y-p2A.y*p2B.x))/den;
	let y = ((p1A.x*p1B.y-p1A.y*p1B.x)*(p2A.y-p2B.y) - (p1A.y-p1B.y)*(p2A.x*p2B.y-p2A.y*p2B.x))/den;

	return createVector(x, y);
}

function orth_by_point(vector, point, a = 1, mag = 1) {
	return vector.copy().rotate(a*HALF_PI).setMag(mag).add(point)
}