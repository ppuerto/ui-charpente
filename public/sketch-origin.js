// https://editor.p5js.org/jhedberg/sketches/rk8ydh6s7
// http://haptic-data.com/toxiclibsjs/examples/polygon-clipping-p5
// |---> http://haptic-data.com/toxiclibsjs/examples/line2d-intersection

let nodeA;
let nodeB;
let origin;

let nodes = [];

let slider;

function setup() {
  // console.log(Math.atan2(5, 2));

  createCanvas(windowWidth, windowHeight);

  slider = createSlider(0, 500, 100);
  slider.position(50, 50);

  slider.size(200);
  slider.style('width', '80px');
  slider.style('background', 'red');

  origin = new Draggable(0.15*width, 0.15*height);
  nodeA = new Draggable(200, 0).add(origin);
  nodeB = new Draggable(0, 150).add(origin);

  nodes.push(origin)
  nodes.push(nodeA)
  nodes.push(nodeB)
}

function draw() {
  background(255, 255);

  let h = slider.value();

  for (let node of nodes) {
    node.over();
  	node.update();
  	node.show();
  }

  let orthA = orth_by_point(p5.Vector.sub(nodeA, origin), nodeA);
  let orthB = orth_by_point(p5.Vector.sub(nodeB, origin), nodeB);

  inter = intersect_point(nodeA, orthA, nodeB, orthB);
  ellipse(inter.x, inter.y, 10);

  elevO = orth_by_point(p5.Vector.sub(origin, inter), inter, a=1, mag=h);
  ellipse(elevO.x, elevO.y, 10);

  elevA = orth_by_point(p5.Vector.sub(nodeA, inter), inter, a=1, mag=h);
  ellipse(elevA.x, elevA.y, 10);

  elevB = orth_by_point(p5.Vector.sub(nodeB, inter), inter, a=-1, mag=h);
  ellipse(elevB.x, elevB.y, 10);

  lds = orth_by_point(p5.Vector.sub(inter, origin), inter);

  inter_lds_a = intersect_point(inter, lds, origin, nodeA);
  ellipse(inter_lds_a.x, inter_lds_a.y, 5);

  inter_lds_b = intersect_point(inter, lds, origin, nodeB);
  ellipse(inter_lds_b.x, inter_lds_b.y, 5);

  orth_arr = orth_by_point(p5.Vector.sub(origin, elevO), inter);
  ep_arr = intersect_point(inter, orth_arr, origin, elevO);
  ellipse(ep_arr.x, ep_arr.y, 5);

  len_ep_arr = p5.Vector.sub(inter, ep_arr).mag();
  vpb = p5.Vector.sub(origin, inter).setMag(len_ep_arr).add(inter);
  ellipse(vpb.x, vpb.y, 5);

  len_ce_a = p5.Vector.sub(nodeA, elevA).mag();
  len_ce_b = p5.Vector.sub(nodeB, elevB).mag();

  herse_a = p5.Vector.sub(nodeA, inter).setMag(-len_ce_a).add(nodeA)
  ellipse(herse_a.x, herse_a.y, 5);

  herse_b = p5.Vector.sub(nodeB, inter).setMag(-len_ce_b).add(nodeB)
  ellipse(herse_b.x, herse_b.y, 5);

  // a1 = Math.atan2(inter.x - ep_arr.x, inter.y - ep_arr.y);
  // a2 = Math.atan2(inter.x - vpb.x, inter.y - vpb.y);

  draw_line(origin, nodeA, s=3);
  draw_line(origin, nodeB, s=3);
  draw_line(origin, inter);
  draw_line(nodeA, inter);
  draw_line(nodeB, inter);
  draw_line(inter, elevA);
  draw_line(inter, elevB);
  draw_line(inter, elevO);
  draw_line(nodeA, elevA);
  draw_line(nodeB, elevB);
  draw_line(origin, elevO);

  draw_line(inter, ep_arr);
  draw_line(inter_lds_a, vpb);
  draw_line(inter_lds_b, vpb);

  draw_line(herse_a, origin);
  draw_line(herse_b, origin);
  draw_line(herse_a, nodeA);
  draw_line(herse_b, nodeB);

  draw_lerp(inter_lds_a, inter_lds_b);
  draw_lerp(origin, inter_lds_a);
  draw_lerp(origin, inter_lds_b);

  // noFill();
  // arc(inter.x, inter.y, 2*len_ep_arr, 2*len_ep_arr, a2-PI/2, a1-PI/2);
}

function mousePressed() {
  for (let node of nodes)  {
    node.pressed();
  }
}

function mouseReleased() {
  for (let node of nodes)  {
    node.released();
  }
}